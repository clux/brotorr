var torrentStream = require('torrent-stream');
var join = require('path').join;
var bytes = require('pretty-bytes');
var prettySeconds = require('pretty-seconds');
var singleLog = require('single-line-log').stdout;
var fs = require('fs');

var link = process.argv[2];
var dest = process.argv[3];
if (!fs.existsSync(dest) || !fs.statSync(dest).isDirectory()) {
  console.error('Destination "' + dest + '" does not exist');
  process.exit(1);
}

// start download
var timeStart = (new Date()).getTime();
var engine = torrentStream(link, {
  connections: 100,     // Max amount of peers to be connected to
  uploads: 0,           // Number of upload slots
  tmp: join(dest, '/tmp'),
  path: dest,           // Where to save the files. Overrides `tmp`
  verify: true,         // Verify previously stored data before starting
  dht: true,            // Whether or not to use DHT to initialize the swarm
  tracker: true,        // Whether or not to use trackers from torrent file or magnet link
  trackers: [],         // Allows to declare additional custom trackers to use
});


var eta = (size, completed, speed) => {
  if (completed > 0 && speed > 0) {
    var bytesRemaining = size - completed;
    var elapsed = (new Date()).getTime() - timeStart;
    var msRemaining = elapsed / completed * bytesRemaining;
    return 'Estimated ' + prettySeconds(~~(msRemaining / 1000)) + ' remaining';
  }
  return '';
};

var bar = (p) => Array.from({length: 20}, (v, k) => k < ~~(p / 5) ? '=' : ' ').join('');

engine.on('ready', () => {
  var totalSize = engine.files.reduce((acc, file) => acc + file.length, 0);
  console.log('Files in torrent:');
  engine.files.forEach((file) => {
    console.log(' -', file.name, bytes(file.length));
    file.select();
  });
  console.log();

  engine.on('upload', (idx) => {
    console.warn('Uploaded piece', idx);
    process.exit(1);
  });

  // progress, via restarted resume, or via current run completion
  var piecesDone = 0;
  for (var i = 0; i < engine.torrent.pieces.length; i += 1) {
    if (engine.bitfield.get(i)) { piecesDone += 1; }
  }
  engine.on('download', () => { piecesDone += 1; });

  var status = () => {
    var percentage = (100 * piecesDone / engine.torrent.pieces.length).toPrecision(4);
    var swarm = engine.swarm;
    var estimate = eta(totalSize, swarm.downloaded, swarm.downloadSpeed());
    var peers = swarm.wires.reduce((acc, wire) => acc + (wire.peerChoking ? 0 : 1), 0);
    singleLog(
      'Pieces: ' + piecesDone + ' / ' + engine.torrent.pieces.length + '\n' +
      'Connected to ' + peers + '/' + swarm.wires.length + ' peers\n' +
      'Torrent Size ' + bytes(totalSize) + '\n\n' +
      'Complete: ' + percentage + '%\n' +
      '[' + bar(percentage) + '] (' + bytes(swarm.downloadSpeed()) + '/s)\n' +
      '0%    25   50   75   100%  ' + estimate + '\n'
    );
  };
  setInterval(status, 500);

  engine.on('idle', () => {
    status(); // ensure last printout is accurate before quitting
    process.exit(0);
  });
});
