# brotorr
Process isolated torrent client using [pm2](https://npmjs.org/package/pm2).

## Usage
Set a default `DOWNLOAD_DIR` evar early in your `.bashrc`:

```bash
export DOWNLOAD_DIR="$HOME/Videos"
```

Clone, `npm install`, and you can run `./torrent "magnet:?xt=urn:btih:1234&dn=stuff"`. This will add an active torrent to pm2's process list.

## Helpers
For you `.functions` - which you can `source` from an `xdg-mime` handler:

```bash
brotorr-download () {
  # shellcheck disable=SC2029
  ssh brotorrhost ./brotorr/torrent \""$1"\"
}
```

Set up magnet mime handler:

```bash
sudo cp xdg-handler/brotorr.desktop /usr/share/applications/
cp xdg-handler/magnet ~/local/bin/magnet
xdg-mime default brotorr.desktop x-scheme-handler/magnet
```

You probably want to edit the desktop file a little bit first.
